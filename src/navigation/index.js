import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { StackNavigator, addNavigationHelpers } from 'react-navigation';
import { createReduxBoundAddListener } from 'react-navigation-redux-helpers';
import Screens from '../global/screens';
import Spinner from '../components/Loader';

import ChooseCity from '../scenes/ChooseCity';
import Weather from '../scenes/Weather';

export const AppNavigator = StackNavigator({
  [Screens.SEARCH_CITY]: {
    screen: ChooseCity,
  },
  [Screens.WEATHER]: {
    screen: Weather
  }
}, {
  navigationOptions: {
    gesturesEnabled: false,
    headerTitleStyle: {
      fontFamily: 'Dosis',
      fontSize: 24
    }
  },
});
const addListener = createReduxBoundAddListener('root');

type Props = {
  navigation: Object,
  loading: Boolean,
  dispatch: Function
}

function Navigator(props: Props) {
  const { dispatch, loading, navigation: state } = props;

  return (
    <View style={{ flex: 1 }} >
      {loading && <Spinner />}
      <AppNavigator
        navigation={
          addNavigationHelpers({
            dispatch,
            state,
            addListener
          })
        }
      />
    </View>
  );
}
export default connect(state => ({
  navigation: state.navigation,
  loading: state.user.loading
}))(Navigator);
