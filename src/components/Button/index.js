import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import styles from './styles';

type Props = {
  title: String,
  onPress: Function,
  disabled?: Boolean
}

export default function Button({ title, onPress, disabled }: Props) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, { backgroundColor: disabled ? '#CDCDC0' : '#7CAA2D' }]}
      disabled={disabled}
    >
      <Text>{title}</Text>
    </TouchableOpacity>
  );
}
Button.defaultProps = {
  disabled: false
};

