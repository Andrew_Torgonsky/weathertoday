import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: 120,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5
  },
  disabled: {
    backgroundColor: 'gray'
  }
});
