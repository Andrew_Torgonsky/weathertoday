import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import SearchBlock from '../SearchBlock';

type Props = {
  cities: Array,
  selectCity: Function
}

export default class SearchList extends PureComponent<Props> {
  render() {
    const { cities, selectCity } = this.props;
    return (
      <FlatList
        data={cities}
        renderItem={({ item }) => (
          <SearchBlock item={item} selectCity={selectCity} />
        )}
        keyboardShouldPersistTaps="handled"
        style={{ width: 250 }}
      />
    );
  }
}
