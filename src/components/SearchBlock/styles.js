import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: 'grey',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flex: 1
  },
  text: {
    textAlign: 'left',
  }
});
