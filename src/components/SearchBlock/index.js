import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import styles from './styles';

type Props = {
  item: Object,
  selectCity: Function
}

export default function searchBlock({ item, selectCity }: Props) {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => selectCity(item.key)}
    >
      <Text style={styles.text}>{item.name}</Text>
    </TouchableOpacity>
  );
}
