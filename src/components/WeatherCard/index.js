import React from 'react';
import { View, Image, Text } from 'react-native';
import styles from './styles';
import { images } from '../../global';

type Props = {
  weather?: {
    temperature?: Number,
    icon?: String,
  },
  provider?: String
}

function WeatherCard(props: Props) {
  const {
    temperature, summary, pressure, windSpeed
  } = props.weather;
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }} >
        <Image
          style={styles.image}
          source={images.placeholder}
        />
        <View>
          <Text>Давление {pressure}</Text>
          <Text>{summary}</Text>
          <Text>Cкорость ветра {windSpeed} м/с</Text>
        </View>
      </View>
      <Text style={styles.temperature}>{temperature} C</Text>
      <Text style={styles.providerText}>{props.provider}</Text>
    </View>
  );
}
WeatherCard.defaultProps = {
  weather: {
    icon: images.placeholder,
    temperature: -10
  },
  provider: 'Unknown'
};
export default WeatherCard;
