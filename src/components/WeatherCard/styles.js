import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    width: '100%',
    height: 80,
    backgroundColor: '#97CAEF',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10
  },
  image: {
    width: 60,
    height: 60
  },
  temperature: {
    fontSize: 26,
    fontFamily: 'Dosis'
  },
  providerText: {
    position: 'absolute',
    bottom: 3,
    right: 3,
    fontSize: 10,
    color: 'grey'
  }
});
