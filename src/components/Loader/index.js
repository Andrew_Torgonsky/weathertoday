import React from 'react';
import { View } from 'react-native';
import Spinner from 'react-native-spinkit';
import styles from './styles';

export default function () {
  return (
    <View style={styles.container} >
      <View>
        <Spinner
          isVisible
          type="WanderingCubes"
          color="red"
          size={100}
        />
      </View>
    </View>
  );
}

