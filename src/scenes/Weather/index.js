import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, TouchableOpacity, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import WeatherCard from '../../components/WeatherCard';
import { updateWeather } from '../../redux/actions/weather';
import styles from './styles';

class Weather extends Component {
  static navigationOptions = {
    title: 'Weather Today'
  }

  // TODO: proptypes, use const for navigatioin
  render() {
    const { current, refreshing, navigation } = this.props;
    return (
      <View style={styles.container} >
        <FlatList
          data={Object.keys(current)}
          renderItem={resource => <WeatherCard weather={current[resource.item]} provider={resource.item} />}
          keyExtractor={item => item}
          ItemSeparatorComponent={() => (<View style={styles.separator} />)}
          contentContainerStyle={styles.contentContainer}
          onRefresh={this.props.updateWeather}
          refreshing={refreshing}
        />
        <TouchableOpacity
          onPress={() => navigation.navigate('SEARCH_CITY')}
        >
          <Text>Change City</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  current: state.weather.currently,
  refreshing: state.user.refreshing
});
const mapDispatchToProps = dispatch => bindActionCreators({
  updateWeather
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Weather);
