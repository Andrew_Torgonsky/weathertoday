import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  separator: {
    height: 5
  },
  contentContainer: {
    padding: 6
  }
});
