import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, TextInput, TouchableOpacity, Image } from 'react-native';

import Button from '../../components/Button';
import SearchList from '../../components/SearchList';
import { setSearchInputs, selectLocation, clearSearchInputs, clearLocationsList, getLocationByGeo } from '../../redux/actions/location';
import { loadWeather } from '../../redux/actions/weather';
import styles from './styles';
import Images from '../../global/images';

type Props = {
  setSearchInputs: Function,
  selectLocation: Function,
  search: string,
  cities: Array,
  clearSearchInputs: Function,
  clearLocationsList: Function,
  fetchWeather: Function,
  locationKey: string
}

function chooseCity(props: Props) {
  const {
    search, cities, locationKey
  } = props;
  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.textInput}
            value={search}
            onChangeText={inputValue => props.setSearchInputs(inputValue)}
            placeholder="Find Your City"
          />
          <TouchableOpacity
            onPress={() => {
              if (search) {
                props.clearSearchInputs();
                props.clearLocationsList();
              }
            }}
          >
            <Image source={Images.clearIcon} style={styles.clearIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigator.geolocation.getCurrentPosition(pos => props.getLocationByGeo(pos.coords))}
          >
            <Image source={Images.location} style={styles.clearIcon} />
          </TouchableOpacity>
        </View>
        <SearchList cities={cities} selectCity={props.selectLocation} />
      </View>
      <Button
        title="Next"
        onPress={props.loadWeather}
        disabled={!locationKey}
      />
    </View>
  );
}
chooseCity.navigationOptions = {
  title: 'Find your city'
};
const stateToProps = ({ location, user }) => ({
  cities: location.cities,
  search: location.search,
  locationKey: user.locationKey
});

const actionsToProps = dispatch => bindActionCreators({
  setSearchInputs,
  selectLocation,
  clearSearchInputs,
  clearLocationsList,
  loadWeather,
  getLocationByGeo
}, dispatch);

export default connect(stateToProps, actionsToProps)(chooseCity);
