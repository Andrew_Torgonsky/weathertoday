import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 30
  },
  searchContainer: {
    flex: 1,
    justifyContent: 'space-between'
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#86AC41',
    padding: 5,
    alignItems: 'center'
  },
  textInput: {
    width: 250,
    fontFamily: 'Dosis',
    fontSize: 18
  },
  clearIcon: {
    width: 25,
    height: 25
  }
});
