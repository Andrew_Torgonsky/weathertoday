import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import configStore from './redux/store';
import Navigator from './navigation';
import Spinner from './components/Loader';

export default class App extends Component {
  render() {
  const { persistor, store } = configStore;
    return (
      <Provider store={store}>
        <PersistGate loading={<Spinner />} persistor={persistor}>
          <Navigator />
        </PersistGate>
      </Provider>
    );
  }
}
