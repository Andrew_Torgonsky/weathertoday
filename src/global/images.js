const images = {
  placeholder: require('../../assets/images/weatherPlaceHolder.png'),
  clearIcon: require('../../assets/images/clear_icon.png'),
  location: require('../../assets/images/location_icon.png')
};

export default images;
