import { createReducer } from 'reduxsauce';

import TYPES from '../constants';

const INITIAL_STATE = {
  currently: {},
  hourly: {},
  daily: {}
};

const setWeatherBySource = (state, { payload: { source, weather } }) => ({
  currently: { ...state.currently, [source]: weather.currently },
  hourly: { ...state.hourly, [source]: weather.hourly },
  daily: { ...state.daily, [source]: weather.daily }
});

const HANDLERS = {
  [TYPES.SET_WEATHER_BY_SOURCE]: setWeatherBySource
};

export default createReducer(INITIAL_STATE, HANDLERS);
