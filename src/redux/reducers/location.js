import { createReducer } from 'reduxsauce';

import TYPES from '../constants';

const INITIAL_STATE = {
  search: '',
  cities: []
};

const setSearchInput = (state, { payload }) => ({ ...state, search: payload.search || payload.locationName });

const setLocationsList = (state, { payload }) => ({ ...state, cities: payload.result });

const clearSearchInput = state => ({ ...state, search: '' });

const clearLocationsList = state => ({ ...state, cities: [] });

const HANDLERS = {
  [TYPES.SET_SEARCH_INPUT]: setSearchInput,
  [TYPES.SET_LOCATION_LIST]: setLocationsList,
  [TYPES.CLEAR_SEARCH_INPUT]: clearSearchInput,
  [TYPES.CLEAR_LOCATION_LIST]: clearLocationsList,
  [TYPES.SET_LOCATION]: setSearchInput,
  [TYPES.SELECT_LOCATION]: clearLocationsList
};

export default createReducer(INITIAL_STATE, HANDLERS);
