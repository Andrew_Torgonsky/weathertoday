import { AppNavigator } from '../../navigation';
import Screens from '../../global/screens';

const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams(Screens.SEARCH_CITY));

export default function (state = initialState, action) {
  const nextState = AppNavigator.router.getStateForAction(action, state);
  return nextState || state;
}
