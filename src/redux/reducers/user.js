import { createReducer } from 'reduxsauce';

import TYPES from '../constants';

const INITIAL_STATE = {
  longitude: 0,
  latitude: 0,
  locationKey: '',
  locationName: '',
  loading: false,
  refreshing: false
};

const setLocation = (state, { payload }) => ({
  ...state,
  longitude: payload.longitude || state.longitude,
  latitude: payload.latitude || state.latitude,
  locationKey: payload.locationKey || state.locationKey,
  locationName: payload.locationName || state.locationName,
});

const setLoader = (state, { payload }) => ({ ...state, loading: payload.status });

const setRefreshing = (state, { payload: { value } }) => ({ ...state, refreshing: value });

const HANDLERS = {
  [TYPES.SET_LOCATION]: setLocation,
  [TYPES.SET_LOADER]: setLoader,
  [TYPES.SET_REFRESH]: setRefreshing
};

export default createReducer(INITIAL_STATE, HANDLERS);
