import { combineReducers } from 'redux';

import navigation from './navigation';
import counter from './counter';
import user from './user';
import location from './location';
import weather from './weather';

const rootReducer = combineReducers({
  navigation,
  counter,
  user,
  location,
  weather
});

export default rootReducer;
