import TYPES from '../constants';

export const setLoader = status => ({
  type: TYPES.SET_LOADER,
  payload: { status }
});

export const setLocation = ({
  longitude, latitude, locationKey, locationName
}) => ({
  type: TYPES.SET_LOCATION,
  payload: {
    longitude, latitude, locationKey, locationName
  }
});
