import TYPES from '../constants';

export const loadWeather = () => ({
  type: TYPES.LOAD_WEATHER
});

export const updateWeather = () => ({
  type: TYPES.UPDATE_WEATHER
});

export const setRefreshing = value => ({
  type: TYPES.SET_REFRESH,
  payload: { value }
});

export const setWeatherBySource = ({ source, weather }) => ({
  type: TYPES.SET_WEATHER_BY_SOURCE,
  payload: { source, weather }
});
