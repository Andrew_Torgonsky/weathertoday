import TYPES from '../constants';

export const setSearchInputs = search => ({
  type: TYPES.SET_SEARCH_INPUT,
  payload: { search }
});

export const clearSearchInputs = () => ({
  type: TYPES.CLEAR_SEARCH_INPUT
});

export const setLocationsList = result => ({
  type: TYPES.SET_LOCATION_LIST,
  payload: { result }
});

export const clearLocationsList = () => ({
  type: TYPES.CLEAR_LOCATION_LIST
});

export const selectLocation = locationKey => ({
  type: TYPES.SELECT_LOCATION,
  payload: { locationKey }
});

export const getLocationByGeo = ({ latitude, longitude }) => ({
  type: TYPES.GET_LOCATION_BY_GEO,
  payload: { latitude, longitude }
});
