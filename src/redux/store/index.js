import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import storage from 'redux-persist/lib/storage';
import { persistStore, persistReducer } from 'redux-persist';
import devTools from 'remote-redux-devtools';
import rootSaga from '../sagas';
import { middleware as middlewareNav } from '../../utils/redux';

import reducers from '../reducers';

const persistConfig = {
  key: 'root',
  storage,
};

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();

  const middleware = [sagaMiddleware, middlewareNav];

  const persistedReducer = persistReducer(persistConfig, reducers);

  const store = createStore(
    persistedReducer,
    compose(applyMiddleware(...middleware), global.reduxNativeDevTools ? global.reduxNativeDevTools({ name: 'osome' }) : nope => nope)
  );
  sagaMiddleware.run(rootSaga);

  const persistor = persistStore(store);

  return {
    persistor,
    store
  };
};

export default configureStore();
