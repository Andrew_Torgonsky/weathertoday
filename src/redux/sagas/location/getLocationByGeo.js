import { put } from 'redux-saga/effects';

import googleApi from '../../../services/googleApi';
import { setLocation } from '../../actions/user';

export default function* getLocationByGeo({ payload }) {
  const { latitude, longitude } = payload;

  const result = yield googleApi.getLocationByGeo({ latitude, longitude });

  yield put(setLocation({ latitude, longitude, ...result }));
}
