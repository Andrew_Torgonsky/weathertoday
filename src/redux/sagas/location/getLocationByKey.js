import { put } from 'redux-saga/effects';

import googleApi from '../../../services/googleApi';
import { setLocation } from '../../actions/user';

export default function* getLocationByKey({ payload }) {
  const { locationKey } = payload;

  const result = yield googleApi.getLocationGeo(locationKey);

  yield put(setLocation({ locationKey, ...result }));
}
