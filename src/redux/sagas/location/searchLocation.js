import { put } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import googleApi from '../../../services/googleApi';
import { setLocationsList } from '../../actions/location';

export default function* ({ payload }) {
  const { search } = payload;
  yield delay(500);
  const result = yield googleApi.searchLocation(search);
  yield put(setLocationsList(result));
}
