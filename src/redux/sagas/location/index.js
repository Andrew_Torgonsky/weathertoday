import { takeLatest } from 'redux-saga/effects';

import searchLocation from './searchLocation';
import getLocationByKey from './getLocationByKey';
import getLocationByGeo from './getLocationByGeo';
import TYPES from '../../constants';

export default function* locationSaga() {
  yield takeLatest(TYPES.SET_SEARCH_INPUT, searchLocation);
  yield takeLatest(TYPES.SELECT_LOCATION, getLocationByKey);
  yield takeLatest(TYPES.GET_LOCATION_BY_GEO, getLocationByGeo);
}
