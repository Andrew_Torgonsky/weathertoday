import { takeEvery } from 'redux-saga/effects';

import loadWeather from './loadWeather';
import updateWeather from './updateWeather';
import TYPES from '../../constants';

export default function* weather() {
  yield takeEvery(TYPES.LOAD_WEATHER, loadWeather);
  yield takeEvery(TYPES.UPDATE_WEATHER, updateWeather);
}
