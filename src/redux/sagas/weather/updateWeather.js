import { call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import fetchWeather from './fetchWeather';
import { setRefreshing } from '../../actions/weather';

export default function* loadWeather() {
  yield put(setRefreshing(true));
  yield call(fetchWeather);
  yield delay(500);
  yield put(setRefreshing(false));
}
