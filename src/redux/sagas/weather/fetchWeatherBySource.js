import { put, select } from 'redux-saga/effects';

import { setWeatherBySource } from '../../actions/weather';

export default function* fetchWeatherBySource(source, service) {
  const location = yield select(state => state.user);
  const weather = yield service.getWeather(location);
  yield put(setWeatherBySource({ source, weather }));
}
