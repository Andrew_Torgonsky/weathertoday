import { call } from 'redux-saga/effects';

import fetchWeatherBySource from './fetchWeatherBySource';
import * as services from '../../../services';

export default function* fetchWeather() {
  for (const service in services) {
    yield call(fetchWeatherBySource, service, services[service]);
  }
}
