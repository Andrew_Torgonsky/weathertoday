import { call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { NavigationActions } from 'react-navigation';

import fetchWeather from './fetchWeather';
import { setLoader } from '../../actions/user';

export default function* loadWeather() {
  yield put(setLoader(true));
  yield call(fetchWeather);
  yield put(NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'WEATHER' })],
  }));
  yield delay(500);
  yield put(setLoader(false));
}
