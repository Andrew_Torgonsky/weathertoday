import { fork } from 'redux-saga/effects';

import locationSaga from './location';
import weatherSaga from './weather';

export default function* rootSaga() {
  yield fork(locationSaga);

  yield fork(weatherSaga);
}
