import { create } from 'apisauce';

class WeatherNsu {
  constructor() {
    this.api = create({
      baseURL: 'http://weather.nsu.ru'
    });
  }

  getWeather() {
    const today = new Date();
    const endPoint = `/loadata.php?tick=${Number.parseInt(today.getTime() / 1000, 10)}&rand=${Math.random()}&std=three`;
    return this.api.get(endPoint)
      .then(({ data, ok }) => {
        if (!ok) throw new Error('API ERROR!');
        const match = data.match('cnv.innerHTML = \'(-?)([\\d.]+)&');
        if (match) {
          const minus = Boolean(match[1]);
          const number = parseFloat(match[2]);
          const deg = number * (minus ? -1 : 1);
          return {
            currently: { temperature: deg }
          };
        }
        return {};
      });
  }
}

export const weatherNsu = new WeatherNsu();
