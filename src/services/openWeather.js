import { create, DEFAULT_HEADERS } from 'apisauce';

import { openWeatherKey } from './config';

const processData = data => ({
  temperature: data.main.temp,
  temperatureHigh: data.main.temp_max,
  temperatureLow: data.main.temp_min,
  humidity: data.main.humidity,
  windSpeed: data.wind.speed,
  windBearing: data.wind.deg,
  summary: data.weather.description,
  icon: data.weather.icon,
  pressure: data.main.pressure,
});

class OpenWeather {
  constructor() {
    this.api = create({
      baseURL: 'https://api.openweathermap.org/data/2.5',
      headers: DEFAULT_HEADERS
    });
    this.key = openWeatherKey;
  }

  getWeather({ latitude, longitude }) {
    return this.api.get('/weather', {
      lat: latitude,
      lon: longitude,
      lang: 'ru',
      units: 'metric',
      APPID: this.key
    })
      .then(({ ok, data, problem }) => {
        if (!ok) throw new Error('API ERROR!');
        return {
          currently: processData(data)
        };
      });
  }
}

export const openWeather = new OpenWeather();
