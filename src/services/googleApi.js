import { create, DEFAULT_HEADERS } from 'apisauce';

import { googleApiKey } from './config';

class GoogleAPI {
  constructor() {
    this.api = create({
      baseURL: 'https://maps.googleapis.com/maps/api',
      headers: DEFAULT_HEADERS
    });
    this.key = googleApiKey;
  }

  searchLocation(name) {
    return this.api.get('/place/autocomplete/json', {
      input: name,
      types: '(cities)',
      language: 'ru',
      key: this.key
    }).then(({ ok, data }) => {
      if (!ok) throw new Error('API ERROR!');
      return data.predictions.map(location => ({
        key: location.place_id,
        name: location.description
      }));
    });
  }

  getLocationGeo(locationKey) {
    return this.api.get('/place/details/json', {
      placeid: locationKey,
      key: this.key,
      language: 'ru'
    }).then(({ ok, data }) => {
      if (!ok) throw new Error('API ERROR!');
      return {
        latitude: data.result.geometry.location.lat,
        longitude: data.result.geometry.location.lng,
        locationName: data.result.name,
      };
    });
  }

  getLocationByGeo({ latitude, longitude }) {
    return this.api.get('geocode/json', {
      latlng: `${latitude},${longitude}`,
      result_type: 'locality',
      key: this.key
    }).then(({ ok, data }) => {
      if (!ok) throw new Error('API ERROR!');
      const location = data.results[0];
      if (!location) return null;
      return {
        locationName: location.formatted_address,
        locationKey: location.place_id
      };
    });
  }
}

export default new GoogleAPI();
