import { create, DEFAULT_HEADERS } from 'apisauce';

import { accuweatherKey } from './config';

class AccuWeather {
  constructor() {
    this.api = create({
      baseURL: 'http://dataservice.accuweather.com',
      headers: DEFAULT_HEADERS
    });
    this.key = accuweatherKey;
  }

  searchLocation(name) {
    return this.api.get('/locations/v1/cities/autocomplete', {
      apikey: this.key,
      q: name
    }).then(({ ok, data }) => {
      if (!ok) throw new Error('API ERROR!');
      return data.map(location => ({
        key: location.key,
        name: location.LocalizedName,
        location: `${location.AdministrativeArea.LocalizedName}, ${location.Country.LocalizedName}`
      }));
    });
  }

  getLocationGeo(locationKey) {
    return this.api.get(`/locations/v1/${locationKey}`, {
      apikey: this.key
    }).then(({ ok, data }) => {
      if (!ok) throw new Error('API ERROR!');
      return {
        latitude: data.GeoPosition.Latitude,
        longitude: data.GeoPosition.Longitude
      };
    });
  }

  getLocationOneDayForecasts(locationKey) {
    return this.api.get(`/forecasts/v1/daily/1day/${locationKey}`, {
      apikey: this.key
    }).then(({ ok, data }) => {
      if (!ok) throw new Error('API ERROR!');
      return data;
    });
  }
}

export default new AccuWeather();
