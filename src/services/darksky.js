import { create, DEFAULT_HEADERS } from 'apisauce';

import { darkSkyKey } from './config';

const processData = data => ({
  temperature: data.temperature,
  temperatureHigh: data.temperatureHigh,
  temperatureLow: data.temperatureLow,
  humidity: data.humidity,
  windSpeed: data.windSpeed,
  windBearing: data.windBearing,
  summary: data.summary,
  icon: data.icon,
  pressure: data.pressure
});

class DarkSky {
  constructor() {
    this.api = create({
      baseURL: `https://api.darksky.net/forecast/${darkSkyKey}`,
      headers: DEFAULT_HEADERS
    });
  }

  getWeather({ latitude, longitude }) {
    return this.api.get(`/${latitude},${longitude}`, { lang: 'ru', units: 'auto' })
      .then(({ ok, data }) => {
        if (!ok) throw new Error('API ERROR!');
        return {
          currently: processData(data.currently),
          hourly: data.hourly.data.map(processData),
          daily: data.daily.data.map(processData)
        };
      });
  }
}

export const darkSky = new DarkSky();
